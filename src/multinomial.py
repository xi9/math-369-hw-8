from functools import reduce
from math import comb, log10, nan
from typing import *
from sys import stdout

# Matthew Wilson, Keegan Erickson, Ian

# There's only about 90 lines of actual code, and the rest is comments


def multinomial(m: int, n: int):
    '''
    outputs the multinomial expansion as a stream
    '''
    for part in multinomial_str_parts(m, n):
        stdout.write(part)


def multinomial_str(m: int, n: int) -> str:
    '''
    A function that returns groups of the multinomial expansion (where
    n₁ + n₂ + ⋯ + nₘ = n):

    (x₁ + x₂ + ⋯ + xₘ)ⁿ = ∑ (ⁿ ₙ₁, ₙ₂, ⋯, ₙₘ) x₁ⁿ¹ x₂ⁿ² ⋯ xₘⁿ

    as a string.
    '''
    return reduce(lambda a, b: f'{a}{b}',
                  multinomial_str_parts(m, n))


def multinomial_str_parts(m: int, n: int) -> Iterable[str]:
    # This function essentially has three parts:
    # 0.) handle the edge cases where m, n = 0 or 1
    # 1.) generate the unique partitions of n
    # 1.) for each unique partition, get the multinomial coefficient
    # 3.) for each unique permutation of a unique partition, generate the
    #     terms x₁ⁿ¹ x₂ⁿ² ⋯ xₘⁿ and aggregate them
    # 4.) aggregate with the multinomial coefficient if it's greater than one
    # 5.) return the result
    # I wish that python didn't have the GIL so that we could dispatch the work
    # to multiple processors, so a generator expression will have to do.
    def add_terms(partition: Tuple[int, ...]):
        started = False
        for term in bare_terms_from_partition(partition):
            if not started:
                started = True
                yield term
            else:
                yield f' + {term}'

    if is_multinomial_edge_case(m, n):
        return str(handle_multinomial_edge_case(m, n))

    for partition in m_partitions(n, m):
        coefficient = multinomial_coefficient(partition)
        if coefficient > 1:
            yield f'+ {coefficient}('
            for term in add_terms(partition):
                yield term
            yield ')\n'
        else:
            for term in add_terms(partition):
                yield term
            yield '\n'


def is_multinomial_edge_case(m: int, n: int) -> bool:
    return not(m and n)


def handle_multinomial_edge_case(m: int, n: int) -> Union[int, float]:
    if not m:
        if not n:
            return nan
        return 0
    return 1


def m_partitions(n: int, m: Optional[int] = None) -> Iterator[Tuple[int, ...]]:
    '''
    A generator modifier that ensures that n-partition tuples are of length m.
    If m is not specified, m = n.

    ex.
    [p for p in m_partitions(5)]
    returns
    [(5, 0, 0, 0, 0),
     (1, 4, 0, 0, 0),
     (1, 1, 3, 0, 0),
     (1, 1, 1, 2, 0),
     (1, 1, 1, 1, 1),
     (1, 2, 2, 0, 0),
     (2, 3, 0, 0, 0)]

    '''
    if m is None:
        m = n
    return (p + tuple(0 for _i in range(m - len(p)))
            for p in partitions(n)
            if not len(p) > m)


def partitions(n: int, I: int = 1) -> Iterator[Tuple[int, ...]]:
    '''
    a recursive generator function which generates the set of tuples containing
    unique addends that sum to n.

    ex.
    [p for p in partitions(5)]
    returns
    [(5,), (1, 4), (1, 1, 3), (1, 1, 1, 2), (1, 1, 1, 1, 1), (1, 2, 2), (2, 3)]
    '''
    yield(n,)
    i: int
    for i in range(I, n // 2 + 1):
        p: Tuple[int, ...]
        for p in partitions(n - i, i):
            yield (i,) + p


def multinomial_coefficient(n_is: Tuple[int, ...]) -> int:
    '''
    Calculates the multinomial coefficient given a list of n_i's that sum to n,
    using binomial coefficients.
    '''
    n: int = sum(n_is)
    output: int = 1
    for number in n_is:
        output *= comb(n, number)
        n -= number
    return output


def bare_terms_from_partition(partition: Tuple[int, ...]) -> Iterable[str]:
    return (bare_term_from_unique(u) for u in unique_permutations(partition))


def bare_term_from_unique(unique: Tuple[int, ...]):
    '''
    Takes a unique permutation of an m_partition of n
    and generates a 'bare' term of x₁ⁿ¹ x₂ⁿ² ⋯ xₘⁿ
    (i.e. without a multinomial coefficient)

    This is done so that we only calculate the multinomial coefficient once
    when we come up with the m_partition, rather than every time that we go
    a unique permutation of that m_partition, where only the 'bare' terms
    change.

    ex.
    bare_term_from_unique((5, 7, 9, 7, 5))
    returns
    x₁⁵x₂⁷x₃⁹x₄⁷x₅⁵
    '''
    return reduce(
        lambda a, b: f'{a}{b}',
        (
            f'x{to_subscript(m + 1)}{to_superscript(n) if n != 1 else ""}'
            for m, n in enumerate(unique)
            if n
        ))


def to_subscript(n: int) -> str:
    '''
    Creates a string of subscripted numbers from an integer
    '''
    subscripts = ('₀', '₁', '₂', '₃', '₄', '₅', '₆', '₇', '₈', '₉')
    return character_mapping_to_str(
        digits(n), subscripts)


def to_superscript(n: int) -> str:
    '''
    Creates a string of superscripted numbers from an integer
    '''
    superscripts = ('⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹')
    return character_mapping_to_str(
        digits(n), superscripts)


def character_mapping_to_str(ns, ds) -> str:
    # I wish there were a more elegant way to do this, but all the superscripts
    # are non-sequential in unicode, as opposed to what they represent.
    # The subscripts are fine however and could be generated a lot faster by
    # using the binary values-- but I'm doubtful this mapping really is that
    # intense compared to generating permutations.
    return reduce(lambda x, y: f'{x}{y}',
                  map(lambda d: ds[d], ns))


# I did have this as recursive, but this function is on a particularly hot codepath
# and the overhead from recursive calls got to be a bit much for the interpreter with
# some large numbers.

def digits(n: int) -> Iterable[int]:
    '''
    Function that returns the digits of an integer
    '''
    order = int(log10(n))
    return (n // 10**(order - p) % 10 for p in range(order + 1))


# This is punching a little above my pay-grade as I haven't read much of
# Knuth's work. There's some discussion of the time complexity of this function,
# and from what I understand this is O(1) time if every element in seq is unique,
# but up to O(n) time if every element in seq is the same (worst case).
# In any case, it's better than generating all the permutations using itertools
# and putting them in a set to eliminate the non-unique ones.

# Slightly modified from
# https://stackoverflow.com/questions/12836385/how-can-i-interleave-or-create-unique-permutations-of-two-strings-without-recur/12837695#12837695
# to return tuple copies


def unique_permutations(seq) -> Iterable[Any]:
    """
    Yield only unique permutations of seq in an efficient way.

    A python implementation of Knuth's "Algorithm L", also known from the
    std::next_permutation function of C++, and as the permutation algorithm
    of Narayana Pandita.
    """
    # Precalculate the indices we'll be iterating over for speed
    i_indices = tuple(range(len(seq) - 1, -1, -1))
    k_indices = i_indices[1:]
    # The algorithm specifies to start with a sorted version
    seq = sorted(seq)
    while True:
        yield tuple(seq)
        # Working backwards from the last-but-one index,           k
        # we find the index of the first decrease in value.  0 0 1 0 1 1 1 0
        for k in k_indices:
            if seq[k] < seq[k + 1]:
                break
        else:
            # else is executed only if the break statement was never reached.
            # If this is the case, seq is weakly decreasing, and we're done.
            return
        # Get item from sequence only once, for speed
        k_val = seq[k]
        # Working backwards starting with the last item,           k     i
        # find the first one greater than the one at k       0 0 1 0 1 1 1 0
        for i in i_indices:
            if k_val < seq[i]:
                break
        # Swap them in the most efficient way
        (seq[k], seq[i]) = (seq[i], seq[k])
        #       k     i
        # 0 0 1 1 1 1 0 0
        # Reverse the part after but not                           k
        # including k, also efficiently.                     0 0 1 1 0 0 1 1
        seq[k + 1:] = seq[-1:k:-1]
